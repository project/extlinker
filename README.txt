CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Extlinker is a module for adding a target="_blank" attribute to the external links in your body content. It requires no special filter and works directly on the content saved to the database.

 * For a full description of the module visit:
   https://www.drupal.org/project/extlinker

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/extlinker


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

Notes

Extlinker currently doesn't have any way to update existing content. If you've got existing content you'll need to resave it for Extlinker to trigger after it's been enabled.

INSTALLATION
------------

 * Install the External Body Linker module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

	1. Enable the module.
	2. Configure your internal domains via: /admin/config/content/extlinker
	3. Write content and save the node.


MAINTAINERS
-----------

 * Carwin Young (carwin) - https://www.drupal.org/u/carwin
