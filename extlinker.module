<?php

/**
 * @file
 * External Body Linker module functions.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Utility\Html;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function extlinker_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.floating_social_icons':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Extlinker is a module for adding a target="_blank" attribute to the external links in your body content.') . '</p>';
      $output .= '<p>' . t('It requires no special filter and works directly on the content saved to the database.') . '</p>';
      $output .= '<h3>' . t('Configuration') . '</h3>';
      $output .= '<p>' . t('Enable the module.') . '</p>';
      $output .= '<p>' . t('Configure your internal domains via: /admin/config/content/extlinker.') . '</p>';
      $output .= '<p>' . t('Write content and save the node.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_entity_presave().
 */
function extlinker_entity_presave(EntityInterface $entity) {
  if ($entity->getEntityTypeId() == 'node') {
    $content = $entity->get('body')->getValue()[0]['value'];

    // Load the body value as an object.
    $dom = Html::load($content);

    // Grab every <a> element in the body value.
    $links = $dom->getElementsByTagName('a');

    // Loop over each anchor so we can modify their target values if necessary.
    foreach ($links as $link) {
      $href = $link->getAttribute('href');

      // If there's no href attribute, just skip it.
      if (!$href) {
        continue;
      }

      // If the href value is not one of our set 'internal urls', set the
      // target to _blank.
      if (extlinker_url_external($href)) {
        $link->setAttribute('target', '_blank');
      }

    }

    // Turn the $dom object back into a string.
    $final = Html::serialize($dom);

    // Create a new array for the body field's value.
    // Keep format and summary intact by using their original values.
    $new_body = [
      'value' => $final,
      'format' => $entity->get('body')->format,
      'summary' => $entity->get('body')->summary,
    ];

    // Set the new body value by creating an array of the necessary parts.
    $entity->body->setValue($new_body);
  }
};

/**
 * Check whether a url exists in our list of internal domains.
 *
 * @param string $url
 *   The URL to check against our settings.
 *
 * @return bool
 *   True if URL isn't in our internal list, False if it is.
 */
function extlinker_url_external($url) {
  // Get our base url.
  global $base_url;

  $pattern = "";

  if (empty($pattern)) {
    // Create an empty array to house our list of domains from settings.
    $domains = [];
    // Grab the domains from settings.
    $extlinker_domains = \Drupal::config('extlinker.settings')->get('extlinker_domains');
    // If we we did not get back an array from the line above, set the variable
    // to an empty array so as not to break things.
    $extlinker_domains = is_array($extlinker_domains) ? $extlinker_domains : [];
    // For each of the domains (including our base url).
    foreach (array_merge($extlinker_domains, [$base_url]) as $domain) {
      $domains[] = preg_quote($domain, '#');
    }
    $pattern = '#^(' . str_replace('\*', '.*', implode('|', $domains)) . ')#';
  }

  // Return whether or not the given argument matches our internal domains, and
  // if it doesn't return that its external.
  return preg_match($pattern, $url) ? FALSE : UrlHelper::isExternal($url);
}
